@extends('layouts.main')

@section('content')

<section>
	      <div class="" style="min-height: 50px;">
        <!-- Jssor Slider Begin -->
        
        <style>
            /* jssor slider loading skin spin css */
            .jssorl-009-spin img {
                animation-name: jssorl-009-spin;
                animation-duration: 1.6s;
                animation-iteration-count: infinite;
                animation-timing-function: linear;
            }

            @keyframes jssorl-009-spin {
                from {
                    transform: rotate(0deg);
                }

                to {
                    transform: rotate(360deg);
                }
            }
        </style>
        <div id="slider1_container" class="training_slider" style="visibility: hidden; position: relative; margin: 0 auto;
        top: 0px; left: 0px; width: 1300px;max-width: 100%; height: 400px; overflow: hidden !important;">
            <!-- Loading Screen -->
            <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;max-width: 100% height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                <img style="margin-top:-19px;position:relative;top:50%;width:38px;max-width: 100%; height:38px;" src="{{asset('/svg/loading/static-svg/spin.svg')}}" />
            </div>

            <!-- Slides Container -->
            <div data-u="slides" style="position: absolute; left: 0px; top: 0px; width: 1400px; height: 500px; overflow: hidden !important;">
                <div>
                    <img data-u="image" src="{{asset('/images/slider/recycle4.jpg')}}" />
                </div>
                <div>
                    <img data-u="image" src="{{asset('/images/slider/recycle2.jpg')}}" />
                </div>
                <div>
                    <img data-u="image" src="{{asset('/images/slider/recycle3.jpg')}}" />
                </div>
                <div>
                    <img data-u="image" src="{{asset('/images/slider/recycle1.jpg')}}" />
                </div>
            </div>

            <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                </svg>
            </div>
            <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                </svg>
            </div>
            <!--#endregion Arrow Navigator Skin End -->
        </div>
        <!-- Jssor Slider End -->
    </div>

    <div class="special_functions">
	<div class="container">
	<div class="row">
		<div class="col-sm-4 exclusive_elements">
			<img src="https://ld-wp.template-help.com/woocommerce_prod-16359/v2/wp-content/uploads/2017/09/icon01.png">
			<h3 style="color: #000000;">Waste Management</h3>
			<P style="text-align: center !important;padding: 5px 0;" >Art and recycling goes hand-in-hand. Eco-artists are, nowadays, transforming old, recycled and resued object into amazing pieces of contemporary art.</P>
		</div>
		<div class="col-sm-4 exclusive_elements">
			<img src="https://ld-wp.template-help.com/woocommerce_prod-16359/v2/wp-content/uploads/2017/09/icon02.png">
			<h3 style="color: #000000;">Electronic Waste</h3>
			<P style="text-align: center !important;padding: 5px 0;" >Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, nostrud.</P>
		</div>
		<div class="col-sm-4 exclusive_elements">
			<img src="https://ld-wp.template-help.com/woocommerce_prod-16359/v2/wp-content/uploads/2017/09/icon03.png">
			<h3 style="color: #000000;">Municipal Solid Waste</h3>
			<P style="text-align: center !important;padding: 5px 0;" >Cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum Lorem ipsum.</P>
		</div>
	</div>
</div>
	</div>
</section>
<section class="top_products">
	<div style="max-width: 68.5%;border:none;" class="container company_overview top_products_res">
		<div>
		<h1>TOP WORKS</h1>
		<p>our top-rated work ideas</p>
	</div>
	<div class="row">
		<div class="col-sm-3">
			<div class="product_items">
				<div class="product_items_image">
					<a href="#"><img src="/images/product1.jpg"></a>
				</div>
					<div class="product_items_details">
						<h3><a href="#">Petite Garnet Necklace Sterling Silver</a></h3>
						<p style="text-align:left;">Sterling silver large hoop style earrings. marked 925 Thailand. great condition.</p>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="product_items">
				<div class="product_items_image">
					<a href="#"><img src="/images/product4.jpg"></a>
				</div>
					<div class="product_items_details">
						<h3><a href="#">Arwen spinner ring, spinning ring</a></h3>
						<p style="text-align:left;">Sterling silver large hoop style earrings. marked 925 Thailand. great condition.</p>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="product_items">
				<div class="product_items_image">
					<a href="#"><img src="/images/product2.jpg"></a>
				</div>
					<div class="product_items_details">
						<h3><a href="#">Raindrops Boho bag, Crossbody bag</a></h3>
						<p style="text-align:left;">Sterling silver large hoop style earrings. marked 925 Thailand. great condition.</p>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="product_items">
				<div class="product_items_image">
					<a href="#"><img src="/images/product3.jpg"></a>
				</div>
					<div class="product_items_details">
						<h3><a href="#">Simulated Aquamarine</a></h3>
						<p style="text-align:left;">Sterling silver large hoop style earrings. marked 925 Thailand. great condition.</p>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="product_items">
				<div class="product_items_image">
					<a href="#"><img src="/images/product1.jpg"></a>
				</div>
					<div class="product_items_details">
						<h3><a href="#">Petite Garnet Necklace Sterling Silver</a></h3>
						<p style="text-align:left;">Sterling silver large hoop style earrings. marked 925 Thailand. great condition.</p>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="product_items">
				<div class="product_items_image">
					<a href="#"><img src="/images/product4.jpg"></a>
				</div>
					<div class="product_items_details">
						<h3><a href="#">Arwen spinner ring, spinning ring</a></h3>
						<p style="text-align:left;">Sterling silver large hoop style earrings. marked 925 Thailand. great condition.</p>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="product_items">
				<div class="product_items_image">
					<a href="#"><img src="/images/product2.jpg"></a>
				</div>
					<div class="product_items_details">
						<h3><a href="#">Raindrops Boho bag, Crossbody bag</a></h3>
						<p style="text-align:left;">Sterling silver large hoop style earrings. marked 925 Thailand. great condition.</p>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="product_items">
				<div class="product_items_image">
					<a href="#"><img src="/images/product3.jpg"></a>
				</div>
					<div class="product_items_details">
						<h3><a href="#">Simulated Aquamarine</a></h3>
						<p style="text-align:left;">Sterling silver large hoop style earrings. marked 925 Thailand. great condition.</p>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<section class="training_works">
	<div style="max-width: 68.5%;border:none;padding: 0px;" class="container company_overview top_works_res">
		<div>
		<h1>COMMING WORKSHOP</h1>
		<p>our workshop training dates</p>
	</div>
	<div class="row">
	<div style="padding-right: 0;" class="col-sm-6 training_content">
				<div class="about_desc special-gift mob_chg">
					<h2>Municipal solid waste management on<br> 16th april 2019</h2>
					<p>Municipal solid waste management in developing countries is very important topic to learn. As well updating the knowledge by the experiences gained from newly practices adopted by the developed country on waste management will be very much helpful to solve effectively the current waste issues in developing countries. To learn the view of the developed country and how they see these related issues in the developing countries and their recommendations and guidance to solve some issues is very interested since i am also is getting involved in this field.</p>
				</div>
			</div>
			<div style="padding-right: 0;" class="col-sm-6 training_content">
				<div class="about_desc special-gift mob_chg">
					<h2>Electronic Waste management on<br>25th April 2019</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse  dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum sunt in culpa qui officia deserunt mollit anim id est laborum sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection