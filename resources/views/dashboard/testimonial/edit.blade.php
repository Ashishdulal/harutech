@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Home</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item">
						<a href="/backoffice/testimonials">testimonials</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">Edit</li>
				</ul>
			</div>
		</div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
		<div class="card-body card-block">
			<form action="/backoffice/testimonials/edit/{{$testimonials->id}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				@csrf
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="name" class=" form-control-label">Name</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="name" id="name" name="name" value="{{$testimonials->name}}" class="form-control">

					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="image" class=" form-control-label">Image input</label><br>
						<span class="au-breadcrumb-span">[ Note: Please upload the image size 800*800 and should be less than 100kb ]</span>
					</div>
					<div class="col-12 col-md-9">
						<input type="file" id="image" accept="image/png, image/jpg, image/jpeg" name="image" class="form-control-file">
						<img src="/uploads/{{$testimonials->image}}" alt="{{$testimonials->name}}">
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="description" class=" form-control-label">Description</label>
					</div>
					<div class="col-12 col-md-9">
						<textarea name="description" id="description" rows="9"  class="form-control ckeditor">{{$testimonials->description}}</textarea>
					</div>
				</div>
				<button type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-dot-circle-o"></i> Update
				</button>
			</form>
		</div>
		<div class="card-footer">

		</div>
	</div>
</div>



</div><!--/.col-->

@endsection