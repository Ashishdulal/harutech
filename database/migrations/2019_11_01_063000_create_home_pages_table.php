<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('logo_image')->nullable();
            $table->string('phone_number')->nullable();
            $table->text('opening_hours')->nullable();
            $table->text('Social_icon_fb')->nullable();
            $table->text('Social_icon_insta')->nullable();
            $table->text('Social_icon_twitter')->nullable();
            $table->text('Social_icon_linkedin')->nullable();
            $table->text('upper_body_image1')->nullable();
            $table->text('upper_body_image2')->nullable();
            $table->longText('upper_body_content')->nullable();
            $table->longText('service_body_content')->nullable();
            $table->longText('staff_body_content')->nullable();
            $table->longText('contact_body_content')->nullable();
            $table->text('lower_body_image1')->nullable();
            $table->longText('lower_body_content')->nullable();
            $table->longText('schedule_content')->nullable();
            $table->longText('why_us_content')->nullable();
            $table->longText('portfolio_content')->nullable();
            $table->text('home_video')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_pages');
    }
}
