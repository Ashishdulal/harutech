<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Newsletter;

class Duplicateemail implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $subscribers = Newsletter::all();
        $count = 0;
        foreach ($subscribers as $subscriber) {
            if($subscriber->email == $value)
            {
                $count += 1;
            }
        }
        if($count > 0){
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The E-mail address has already been subscribed !';
    }
}