@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Home</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">
						<a href="/backoffice/careers">Careers</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row m-t-30">
			<div class="col-md-12 dashboard-space">
				<h4>Banner Image</h4><br>
				<div class="job-banner-image">
					<a href="#" data-toggle="modal" data-target="#myBanner"><img src="/images/banner/{{$jobBanner->jobs_banner}}"><span style="color: #000000;text-align: center;"><?php echo ($jobBanner->jobs_detail) ?></span><br><span style="margin: 20px 0 0 20px;" class="btn btn-primary">Edit Banner Details</span></a>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="myBanner" role="dialog">
					<div class="modal-dialog">
						
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">Careers Banner Image</h4>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
							<div class="modal-body">
								<form method="post" action="/backoffice/banner/image/{{$jobBanner->id}}" enctype="multipart/form-data"> 

									@csrf
									<div class="field">
										<!-- image input-->
										<div class="form-group">
											<label class="control-label" for="jobs_banner">Select Image:</label>
											<div class="">
												<input type="file" onchange="readURL(this);" class="form-control" name="jobs_banner"  accept="image/png, image/jpg, image/jpeg">
												<br><img id="blah" src="/images/banner/{{$jobBanner->jobs_banner}}" alt="Selected Image" />
											</div>
										</div>
										<div class="row form-group">
													<div class="col col-md-12">
														<label for="jobs_detail" class=" form-control-label">Careers Description:</label>
													</div>
													<div class="col-12 col-md-12">
														<textarea name="jobs_detail"  rows="9"  class="form-control ckeditor">{{$jobBanner->jobs_detail}}</textarea>
														
													</div>
												</div>
										<button style="float: left;" type="submit" class="btn btn-primary">submit</button></div>
										<a href="#" style="float: left;margin-left: 10px;" class="btn btn-danger" data-dismiss="modal">Close</a>
									</form>
								</div>
								<div class="modal-footer">
								</div>
							</div>
							
						</div>
					</div>
					<br>
					<a style="margin: 0 0 20px 20px;" href="/backoffice/careers/create" class="btn btn-primary">Add New Career</a>
					<!-- DATA TABLE-->
					<div class="table-responsive m-b-40">
						<table class="table table-borderless table-data3">
							<thead>
								<tr>
									<th>Id</th>
									<th>Careers Name</th>
									<th>Description</th>
									<th>Image</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@if(count($jobs))
								@foreach($jobs as $job)
								<tr>
									<td>{{$job->id}}</td>
									<td>{{$job->name}}</td>
									<td><?php echo ($job->description)?></td>
									<td class="process"><img src="/uploads/{{$job->image}}"></td>
									<td><a href="{{route('job.edit', $job->id)}}"><button class="btn btn-primary make-btn">Edit</button></a>|
										<form method="post" action="{{route('job.delete',$job->id)}}">
											@csrf
											{{ method_field('DELETE') }}
											<button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
										</form>
									</td>
								</tr>
								@endforeach
								@else
								<tr>
									<td>
										<h5>There are no Careers added.</h5>
									</td>
								</tr>
								
								@endif
							</tbody>
						</table>
					</div>
					<!-- END DATA TABLE-->
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
		if(! result){
			evt.stopPropagation();
			evt.preventDefault();	
		}
	}
</script>

@endsection