<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Menu;
use App\Service;
use App\User;
use App\PageSetting;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pageSetting = PageSetting::findOrFail('1');
        $users = User::count();
        $menus = Menu::count();
        $services = Service::count();
        $blogs = Blog::count();
        return view('home',compact('users','menus','services','blogs','pageSetting'));
    }
}
