<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Jobs_detail;
use File;
use App\Blogcategory;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::all();
        $blogBanner = Jobs_detail::findOrFail('1');
        $blogcategories = Blogcategory::all();
        return view('dashboard.blog.blogs.index', compact('blogs','blogcategories','blogBanner')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $blogcategories = Blogcategory::all();
        return view ('dashboard.blog.blogs.create', compact('blogcategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $blogs = new Blog();
        $blogs->title = request('title');
        $blogs->cat_id = request('cat_id');
        $blogs->description = request('description');
        $request->validate([
            'title' => ['required', 'min:5'],
            'description' => ['required', 'min:10'],
            'f_image' => 'image|mimes:jpg,png,jpeg|',
            'i_image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        if(file_exists($request->file('f_image'))){
            $image = $request->file('f_image');
            $imageName =  "blog".time().'.'.$request->file('f_image')->getClientOriginalName();
            $image->move(public_path('uploads'),$imageName);
            $blogs->f_image = $imageName;
        }
        else{
            $blogs->f_image = 'default-thumbnail.png';
        }
        if(file_exists($request->file('i_image'))){
            $image = $request->file('i_image');
            $iimageName =  "blog".time().'.'.$request->file('i_image')->getClientOriginalName();
            $image->move(public_path('uploads'),$iimageName);
            $blogs->i_image = $iimageName;
        }
        else{
            $blogs->i_image = 'default-thumbnail.png';
        }
        $blogs->save();
        return redirect('/backoffice/news');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog,$id)
    {
        $blogs = Blog::findOrFail($id);
        $blogcategories = Blogcategory::all();
        return view('dashboard.blog.blogs.edit', compact('blogs','blogcategories')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog,$id)
    {
        $blogs = Blog::findOrFail($id);
        $blogs->title = request('title');
        $blogs->cat_id = request('cat_id');
        $blogs->description = request('description');
        $request->validate([
            'title' => ['required', 'min:5'],
            'description' => ['required', 'min:10'],
            'f_image' => 'image|mimes:jpg,png,jpeg|',
            'i_image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        if(file_exists($request->file('f_image'))){
            $image = $request->file('f_image');
            $imageName =  "blog".time().'.'.$request->file('f_image')->getClientOriginalName();
            $image->move(public_path('uploads'),$imageName);
            $blogs->f_image = $imageName;
        }
        else{
            $blogs->f_image = $blogs->f_image;
        }
        if(file_exists($request->file('i_image'))){
            $image = $request->file('i_image');
            $iimageName =  "blog".time().'.'.$request->file('i_image')->getClientOriginalName();
            $image->move(public_path('uploads'),$iimageName);
            $blogs->i_image = $iimageName;
        }
        else{
            $blogs->i_image = $blogs->i_image;
        }
        $blogs->save();
        return redirect('/backoffice/news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blogs = Blog::findOrFail($id);
        $image_path = public_path("uploads/".$blogs->f_image);
        if(file_exists($image_path)){
            //File::delete($image_path);
            File::delete( $image_path);
        }
        $image_path = public_path("uploads/".$blogs->i_image);
        if(file_exists($image_path)){
            //File::delete($image_path);
            File::delete( $image_path);
        }
        $blogs->delete();
        return redirect()->back();
    }
}
