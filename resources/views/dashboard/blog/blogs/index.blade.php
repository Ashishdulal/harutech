@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Home</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">
						News Posts
					</li>
				</ul>
			</div>
		</div>
		<div class="row m-t-30">
			<div class="col-md-12 dashboard-space">
				<h4>Banner Image</h4><br>
				<div class="job-banner-image">
					<a href="#" data-toggle="modal" data-target="#myBanner"><img src="/images/banner/{{$blogBanner->blog_banner}}"><span style="margin: 20px 0 0 20px;" class="btn btn-primary">Edit Banner Image</span></a>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="myBanner" role="dialog">
					<div class="modal-dialog">
						
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">News Banner Image</h4>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
							<div class="modal-body">
								<form method="post" action="/backoffice/banner/image/{{$blogBanner->id}}" enctype="multipart/form-data"> 

									@csrf
									<div class="field">
										<!-- image input-->
										<div class="form-group">
											<label class="control-label" for="blog_banner">Select Image:</label>
											<div class="">
												<input type="file" onchange="readURL(this);" class="form-control" name="blog_banner"  accept="image/png, image/jpg, image/jpeg">
												<br><img id="blah" src="/images/banner/{{$blogBanner->blog_banner}}" alt="Selected Image" />
											</div>
										</div>
										<button style="float: left;" type="submit" class="btn btn-primary">submit</button></div>
										<a href="#" style="float: left;margin-left: 10px;" class="btn btn-danger" data-dismiss="modal">Close</a>
									</form>
								</div>
								<div class="modal-footer">
								</div>
							</div>
							
						</div>
					</div>
					<br>
					<a style="margin: 0 0 20px 20px;" href="/backoffice/news/create" class="btn btn-primary">Add New News</a>
					<!-- DATA TABLE-->
					<div class="table-responsive m-b-40">
						@foreach ($blogcategories as $blog_cat)
						<h3>{{$blog_cat->title}}</h3>
						<table class="table table-hover make-gap">

							<thead>
								<tr>
									<th>Id</th>
									<th>Title</th>
									<th>Description</th>
									<th>Images</th>
									<th>Action</th>
									<th>Author</th>
								</tr>
							</thead>
							<tbody>
								@foreach($blogs as $blog)
								@if( $blog_cat->id == $blog->cat_id)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{$blog->title}}</td>
									<td><?php echo ($blog->description)?></td>
									<td class="work-img process">
										<img src="/uploads/{{$blog->f_image}}" alt="{{$blog->title}}">
										<img src="/uploads/{{$blog->i_image}}" alt="{{$blog->title}}">
									</td>
									<td class="make_btn_straight">
										<a href="/backoffice/news/edit/{{$blog->id}}" class="btn btn-primary make-btn">
											Edit
										</a>|
										<form action="{{route('delete-post.blog',$blog->id)}}">
											<button type="submit" class="btn btn-danger" onclick="makeWarning(event)">Delete</button>
										</form>
									</td>
									<td><p>Admin</p></td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
						@endforeach
					</div>
					<!-- END DATA TABLE-->
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
		if(! result){
			evt.stopPropagation();
			evt.preventDefault();	
		}
	}
</script>

@endsection