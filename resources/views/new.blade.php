@extends('layouts.new.app', ['title' => ($pageSetting->site_title)],['discription'=> ($pageSetting->tagline)])
@section('metaDescription')
<meta name="tagline" content="{{$pageSetting->tagline}}">
<meta name="description" content="{{$pageSetting->meta_description_seo}}">
<meta name="site url" content="{{$pageSetting->site_url}}">
<meta name="keywords" content="{{$pageSetting->meta_keywords_seo}}">
@endsection
@section('content')
<section class="section swiper-container swiper-slider swiper-slider-2 slider-scale-effect" data-loop="false" data-autoplay="5500" data-simulate-touch="false" data-slide-effect="fade">
  <div class="swiper-wrapper">
    @foreach($sliders as $slider)
    <div class="swiper-slide">
      <div class="slide-bg" style="background-image: url(&quot;/uploads/{{$slider->image}}&quot;)"></div>
      <div class="swiper-slide-caption section-md">
        <div class="container">
          <div class="row">
            <div class="col-sm-10 col-lg-7 col-xl-6 swiper-caption-inner">
              <h1 data-caption-animate="fadeInUp" data-caption-delay="100"><span class="text-primary">{{$slider->title}}</span><br>{{$slider->title_1}}
              </h1>
              <div class="divider-lg" data-caption-animate="fadeInLeft" data-caption-delay="550"></div>
              <p class="lead" data-caption-animate="fadeInUp" data-caption-delay="250"><?php echo ($slider->description)?></p>
              <a class="button button-default-outline" data-toggle="modal" data-target="#myModal" href="#" data-caption-animate="fadeInUp" data-caption-delay="450">Book Now</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforeach

  </div>
  <!-- Swiper Pagination -->
  <div class="swiper-pagination"></div>
  <div class="swiper-button-prev"></div>
  <div class="swiper-button-next"></div>
</section>
<section class="section section-lg bg-gray-100">
  <div class="container">
    <div class="row row-50 align-items-lg-center justify-content-xl-between">
      <div class="col-lg-6">
        <div class="box-images box-images-modern">
          <div class="box-images-item" data-parallax-scroll="{&quot;y&quot;: -10,   &quot;smoothness&quot;: 30 }"><img src="/uploads/homepage/{{$homepages->upper_body_image1}}" alt="" width="310" height="370"/>
          </div>
          <div class="box-images-item box-images-without-border" data-parallax-scroll="{&quot;y&quot;: 40,  &quot;smoothness&quot;: 30 }"><img src="/uploads/homepage/{{$homepages->upper_body_image2}}" alt="" width="328" height="389"/>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-xl-5 text-justify">
        <p> 
          <?php echo ($homepages->upper_body_content)?></p>
          <a class="button button-default-outline" href="/about-us">read more</a>
        </div>
      </div>
    </div>
  </section>
  <section class="section section-small bg-default text-center">
    <div class="container">
      <h2>Our Services</h2>
      <div class="divider-lg"></div>
      <?php echo ($homepages->service_body_content)?></p>
    </div>
    <div class="container-fluid container-responsive">
      <div class="row no-gutters">
        <?php $count=1; ?>
        @foreach($services as $service)
        @if($count%2  == 0 )
        <div class="col-lg-4">
          <div class="box-service-modern">
            <div class="box-icon-classic box-icon-classic-vertical">
              <div class="icon-classic-aside">
                <h4 class="icon-classic-title"><a href="/services">{{$service->name}}</a></h4>
              </div>
            </div>
            <div class="box-service-modern-img"><img src="/uploads/{{$service->image}}" alt="" width="99%" height="312"/>
            </div>
          </div>
        </div>
        
        @else()
        <div class="col-lg-4">
          <div class="box-service-modern box-service-modern-reverse">
            <div class="box-icon-classic box-icon-classic-vertical">
              <div class="icon-classic-aside">
                <h4 class="icon-classic-title"><a href="/services">{{$service->name}}</a></h4>
              </div>
            </div>
            <div class="box-service-modern-img"><img src="/uploads/{{$service->image}}" alt="" width="99%" height="312"/>
            </div>
          </div>
        </div>
        @endif
        <?php $count ++; ?>
        <?php if($count > 3)
        break
        ?>
        @endforeach
      </div><a class="button button-default-outline" href="/services">View all services</a>
    </div>
  </div>
</section>
<div style="display: none;" class="container text-center">
  <h2>Our Areas Of Expertise</h2>
  <div class="divider-lg"></div>
</div>
<section style="display: none;" class="section parallax-container experience" data-parallax-img="images/parallax-2-1920x380.jpg">
  <div class="parallax-content section-sm  text-center">
    <div class="container">
      <div class="row row-30 counter-list-border">
        <div class="owl-carousel" data-items="3" data-lg-items="7" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false">
          @foreach ($experiences as $experience)
          <div class="col-6 col-md-12">
            <!-- Box counter-->
            <article class="box-counter text-center">
              <div class="quote-image"><img src="/uploads/{{$experience->image}}" alt="" width="100" height="100"/>
              </div>
              <p class="box-counter-title">{{$experience->name}}</p>
            </article>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
<section style="display: none;" class="section section-lg bg-default">
  <div class="container">
    <div class="row row-50">
      <div class="col-sm-6 col-lg-3 text-center text-lg-left">
        <?php echo ($homepages->staff_body_content)?>
      </div>
      <div class="col-sm-6 col-lg-9">
        <p class="text-center text-lg-right"><a class="button-link button-link-icon" href="/about-us">View All Team <span class="icon fa-arrow-right icon-primary"></span></a></p>
        <!-- Owl Carousel-->
        <div class="owl-carousel carousel-inset" data-items="1" data-lg-items="3" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false">
          @foreach($staffs as $staff)
          <div class="team-minimal team-minimal-type-2">
            <figure><img src="/uploads/{{$staff->image}}" alt="" width="370" height="370"></figure>
            <div class="team-minimal-caption">
              <h4 class="team-title"><a href="/about-us#haruyosi_about">{{$staff->name}}</a></h4>
              <p>{{$staff->designation}}</p>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section-transform-bottom">
  <div class="container-fluid section-sm bg-primary context-dark">
    <div style="margin-right: 0px;" class="row justify-content-center row-30">
      <div class="col-sm-10 text-center">
        <h2>Get started with our products, for Quotation</h2>
        <div class="divider-lg"></div>
      </div>
      <div class="col-sm-10 col-lg-6 text-center">
        <p class="block-lg">Contact us to get a free quotation and a Demo Of our product.</p>
        <div class="form-button">
          <button class="button button-primary" data-toggle="modal" data-target="#myModal" href="#" type="submit">MAKE AN APPOINTMENT</button>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section section-small bg-gray-100">
  <div class="container-fluid container-responsive">
    <div class="row row-50 justify-content-center">
      <div class="col-md-6 resp-video">
        <div class="box-video" id="myVideo" data-lightgallery="group">
          <video autoplay="" loop="" muted="" height="100%" width="100%" src="/uploads/homepage/{{$homepages->lower_body_image1}}"></video>
        </div>
      </div>
      <div class="col-md-6 text-justify resp-video">
        <?php echo ($homepages->lower_body_content)?>
      </div>
    </div>
  </div>
</section>
<section style="display: none;" class="section section-xl bg-default">
  <div class="container">
    <div class="row no-gutters pricing-box-modern justify-content-lg-end">
      <div class="col-sm-6 col-lg-4">
        <div class="pricing-box-inner box-left">
          <?php echo ($homepages->schedule_content)?>
          <a class="button-link button-link-icon" href="#" data-toggle="modal" data-target="#myModal">make an appointment  <span class="icon fa-arrow-right icon-primary"></span></a>
        </div>
      </div>
      <div class="d-none d-lg-block col-lg-4 img-wrap"><img src="/uploads/homepage/{{$homepages->contact_body_content}}" alt="" width="498" height="688"/>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="pricing-box-inner bg-primary context-dark box-right">
          <?php echo ($homepages->why_us_content)?>
        </div>
      </div>
    </div>
  </div>
</section>
<section style="display: none;" class="section parallax-container" data-parallax-img="images/parallax-7-1920x1020.jpg">
  <div class="parallax-content section-lg text-center ">
    <div class="container"> 
      <h2>Testimonials</h2>
      <div class="divider-lg"></div>
      <!-- Owl Carousel-->
      <div class="owl-carousel" data-items="1" data-lg-items="3" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false">
        @foreach($testimonials as $testimonial)
        <div class="quote-corporate quote-corporate-center-img">
          <div class="quote-header">
            <h4>{{$testimonial->name}}</h4>
            <p class="big">Client</p>
          </div>
          <div class="quote-body">
            <div class="quote-text">
              <p><?php echo ($testimonial->description)?></p>
            </div>
            <svg class="quote-body-mark" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="66px" height="49px" viewbox="0 0 66 49" enable-background="new 0 0 66 49" xml:space="preserve">
              <g></g>
              <path d="M36.903,49v-3.098c9.203-5.315,14.885-12.055,17.042-20.222c-2.335,1.524-4.459,2.288-6.37,2.288                      c-3.186,0-5.875-1.29-8.071-3.876c-2.194-2.583-3.293-5.74-3.293-9.479c0-4.133,1.443-7.605,4.327-10.407                       C43.425,1.405,46.973,0,51.185,0c4.213,0,7.735,1.784,10.566,5.352C64.585,8.919,66,13.359,66,18.669                       c0,7.482-2.85,14.183-8.549,20.112C51.751,44.706,44.902,48.112,36.903,49z M0.69,49v-3.098                        c9.205-5.315,14.887-12.055,17.044-20.222c-2.335,1.524-4.478,2.288-6.423,2.288c-3.152,0-5.823-1.29-8.02-3.876                        C1.096,21.51,0,18.353,0,14.614c0-4.133,1.434-7.605,4.301-10.407C7.168,1.405,10.709,0,14.92,0c4.247,0,7.778,1.784,10.592,5.352                       c2.814,3.567,4.223,8.007,4.223,13.317c0,7.482-2.843,14.183-8.524,20.112C15.53,44.706,8.69,48.112,0.69,49z"></path>
            </svg>
          </div>
          <div class="quote-image"><img src="/uploads/{{$testimonial->image}}" alt="" width="90" height="90"/>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</section>
<section class="section section-lg bg-default text-center">
  <div class="container">
    <h2>Portfolio</h2>
    <div class="divider-lg"></div>
    <?php echo ($homepages->portfolio_content)?>
  </div>
  <div class="container container-portfolio">
    <div class="col-lg-12">
      <div class="isotope row" data-isotope-layout="masonry" data-isotope-group="gallery" data-lightgallery="group" data-column-class=".col-sm-6.col-lg-4">
        <?php $count=1; ?>
        @foreach ($gallery as $galle)
        <div class="col-sm-6 col-lg-4 isotope-item" data-filter="{{$galle->cat_id}}"><a class="gallery-item"  href="{{$galle->url}}"><img class="port-img" src="/uploads/gallery/{{$galle->file}}" alt="" width="570" height="570"/><span class="gallery-item-title"><div class="form-button1"><i class="fa fa-external-link" aria-hidden="true"></i></div></span><!-- <span class="gallery-item-button"></span> --></a>
        <span style="text-transform: capitalize;" class="gallery-item-title">{{$galle->name}}</span>
      </div>
      <?php $count ++; ?>
      <?php if($count > 6)
      break
      ?>
      @endforeach
    </div>
  </div>

</div>
<a class="button button-default-outline" href="/portfolio">View all portfolio</a>
</section>
<section class="section-transform-bottom">
  <div class="container-fluid section-md bg-primary context-dark">
    <div style="margin-right: 0px;" class="row justify-content-center row-50 home-newsletter">
      <div class="col-sm-10 text-center">
        <h2>Subscribe to Our Newsletter</h2>
        <div class="divider-lg"></div>
      </div>
      <div class="col-sm-10 col-lg-6">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}<br></li>
            @endforeach
          </ul>
        </div>
        @endif
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ $message }}</strong>
        </div>
        @endif
        <!-- RD Mailform-->
        <form class="rd-form-inline" method="post" action="{{url('/subscribe/send')}}">
          @csrf
          <div class="form-wrap">
            <label class="form-label" for="subscribe-form-0-email">{{ __('E-Mail Address') }}</label>
            <input id="subscribe-form-0-email" type="email" class="form-input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

            @error('email')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
          <div class="form-button1">
            <button class="button button-primary" type="submit">Subscribe</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

@endsection