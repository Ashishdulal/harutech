<?php

namespace App\Http\Controllers;

use App\Slider;
use File;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::latest()->get();
        return view('/dashboard/slider/index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('/dashboard/slider/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slider = new Slider();
        $request->validate([
            'name' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg'
        ]);
        $slider->name = $request->name;
        $slider->title = $request->title;
        $slider->title_1 = $request->title_1;
        $slider->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "Slider".time().'.'.$request->file('image')->getclientOriginalName();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $slider->image = $image;
        }
        else{
            $slider->image = 'default-thumbnail.png';
        }        
        $slider->save();
        return redirect('/backoffice/sliders');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider,$id)
    {
        $slider = slider::findOrFail($id);
        return view('/dashboard/slider/edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider,$id)
    {
        $slider = slider::findOrFail($id);

        $request->validate([
            'name' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $slider->name = $request->name;
        $slider->description = $request->description;
        $slider->title = $request->title;
        $slider->title_1 = $request->title_1;
        if(file_exists($request->file('image'))){
            $image = "Slider".time().'.'.$request->file('image')->getclientOriginalName();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $slider->image = $image;
        }
        else{
            $slider->image = $slider->image;
        }        
        $slider->save();
        return redirect('/backoffice/sliders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = slider::findOrFail($id);
        $image_path = public_path("uploads/".$slider->image);
        if(file_exists($image_path)){
            //File::delete($image_path);
            File::delete( $image_path);
        }
        $slider->delete();
        return back();
    }
}
