@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Home</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">Homepage Content</li>
				</ul>
			</div>
		</div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
		<div class="card-body card-block">
			<span class="au-breadcrumb-span note-space">[Note: <a href="#help_image">Click For Help</a>]</span>
			<form action="/backoffice/homepage/edit/{{$homepages->id}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				@csrf
			<div style="text-align: right;margin-bottom: 30px;" class="home-btn">
				<button type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-dot-circle-o"></i> Update
				</button>
			</div>
<!-- 				<div class="row form-group">
					<div class="col col-md-3">
						<label for="logo_image" class=" form-control-label">Logo Image input (1)</label><br>
						<span class="au-breadcrumb-span">[ Note: Please upload the '.png' format image and size should be less than 100kb ]</span>
					</div>
					<div class="col-12 col-md-9 process">
						<input type="file" id="logo_image" accept="image/png, image/jpg, image/jpeg" name="logo_image" class="form-control-file">
						<img src="/uploads/backofficepage/{{$homepages->logo_image}}" alt="{{$homepages->name}}">
					</div>
				</div> -->
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="logo_image" class=" form-control-label">Chat Details (1)</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="name" id="logo_image" name="logo_image" value="{{$homepages->logo_image}}" class="form-control">
						<span class="au-breadcrumb-span">[ Note: Change the fafa class and link only. In default:<br> [ class="icon fa-whatsapp chat-logo" target="_blank" href="https://wa.me/15551234567" ] ]</span>

					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="phone_number" class=" form-control-label">Phone Number (2)</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="name" id="phone_number" name="phone_number" value="{{$homepages->phone_number}}" class="form-control">

					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="opening_hours" class=" form-control-label">Opening Hours (3)</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="name" id="opening_hours" name="opening_hours" value="{{$homepages->opening_hours}}" class="form-control">

					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="Social_icon_fb" class=" form-control-label">Facebook Link (4)</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="name" id="Social_icon_fb" name="Social_icon_fb" value="{{$homepages->Social_icon_fb}}" class="form-control">

					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="Social_icon_insta" class=" form-control-label">Instagram Link (5)</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="name" id="Social_icon_insta" name="Social_icon_insta" value="{{$homepages->Social_icon_insta}}" class="form-control">

					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="Social_icon_twitter" class=" form-control-label">Twitter Link (6)</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="name" id="Social_icon_twitter" name="Social_icon_twitter" value="{{$homepages->Social_icon_twitter}}" class="form-control">

					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="Social_icon_linkedin" class=" form-control-label">Linked Ln link (7)</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="name" id="Social_icon_linkedin" name="Social_icon_linkedin" value="{{$homepages->Social_icon_linkedin}}" class="form-control">

					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="upper_body_image1" class=" form-control-label">Upper Body Image input (8)</label>
						<br>
						<span class="au-breadcrumb-span">[ Note: Please upload the image size 327*388 and should be less than 100kb ]</span>
					</div>
					<div class="col-12 col-md-9 process">
						<input type="file" id="upper_body_image1" accept="image/png, image/jpg, image/jpeg" name="upper_body_image1" class="form-control-file">
						<img src="/uploads/homepage/{{$homepages->upper_body_image1}}" alt="image">
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="upper_body_image2" class=" form-control-label"> Upper Body Next Image input (9)</label><br>
						<span class="au-breadcrumb-span">[ Note: Please upload the image size 310*370 and should be less than 100kb ]</span>
					</div>
					<div class="col-12 col-md-9 process">
						<input type="file" id="upper_body_image2" accept="image/png, image/jpg, image/jpeg" name="upper_body_image2" class="form-control-file">
						<img src="/uploads/homepage/{{$homepages->upper_body_image2}}" alt="Image">
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="upper_body_content" class=" form-control-label">Upper Body Description (10)</label>
					</div>
					<div class="col-12 col-md-9">
						<textarea name="upper_body_content"  rows="9"  class="form-control ckeditor">{{$homepages->upper_body_content}}</textarea>
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="service_body_content" class=" form-control-label">Service Body Description (11)</label>
					</div>
					<div class="col-12 col-md-9">
						<textarea name="service_body_content"  rows="9"  class="form-control ckeditor">{{$homepages->service_body_content}}</textarea>
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="staff_body_content" class=" form-control-label">Staff Body Description (12)</label>
					</div>
					<div class="col-12 col-md-9">
						<textarea name="staff_body_content"  rows="9"  class="form-control ckeditor">{{$homepages->staff_body_content}}</textarea>
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="lower_body_image1" class=" form-control-label">Lower Body Video (13)</label><br>
						<span class="au-breadcrumb-span">[ Note: Please upload the video less than 5-10mb ]</span>
					</div>
					<div class="col-12 col-md-9">
						<input type="file" name="lower_body_image1" class="form-control-file file_multi_video" accept="video/mp4,video/avi,video/mpeg,video/MKV">

						<video style="margin-top: 20px;" width="400" controls>
							<source src="/uploads/homepage/{{$homepages->lower_body_image1}}" id="video_here">
								Your browser does not support HTML5 video.
							</video>
						</div>
					</div>
					<div class="row form-group">
						<div class="col col-md-3">
							<label for="lower_body_content" class=" form-control-label ckeditor">Lower body Description (14)</label>
						</div>
						<div class="col-12 col-md-9">
							<textarea name="lower_body_content"  rows="9"  class="form-control ckeditor">{{$homepages->lower_body_content}}</textarea>
						</div>
					</div>
					<div class="row form-group">
						<div class="col col-md-3">
							<label for="schedule_content" class=" form-control-label ckeditor">Schedule Description (15)</label>
						</div>
						<div class="col-12 col-md-9">
							<textarea name="schedule_content"  rows="9"  class="form-control ckeditor">{{$homepages->schedule_content}}</textarea>
						</div>
					</div>
					<div class="row form-group">
						<div class="col col-md-3">
							<label for="contact_body_content" class=" form-control-label"> Schedule & Why Us Image input (16)</label><br>
						<span class="au-breadcrumb-span">[ Note: Please upload the image size 457*631 and should be less than 100kb ]</span>
						</div>
						<div class="col-12 col-md-9 process">
							<input type="file" id="contact_body_content" accept="image/png, image/jpg, image/jpeg" name="contact_body_content" class="form-control-file">
							<img src="/uploads/homepage/{{$homepages->contact_body_content}}" alt="Image">
						</div>
					</div>
					<div class="row form-group">
						<div class="col col-md-3">
							<label for="why_us_content" class=" form-control-label ckeditor">Why Us Description (17)</label>
						</div>
						<div class="col-12 col-md-9">
							<textarea name="why_us_content"  rows="9"  class="form-control ckeditor">{{$homepages->why_us_content}}</textarea>
						</div>
					</div>
					<div class="row form-group">
						<div class="col col-md-3">
							<label for="portfolio_content" class=" form-control-label">Portfolio Description (18)</label>
						</div>
						<div class="col-12 col-md-9">
							<textarea name="portfolio_content"  rows="9"  class="form-control ckeditor">{{$homepages->portfolio_content}}</textarea>
						</div>
					</div>
					<button type="submit" class="btn btn-primary btn-sm">
						<i class="fa fa-dot-circle-o"></i> Update
					</button>
				</form>
			</div>
			<div id="help_image" class="card-footer">
				<span class="au-breadcrumb-span">[The below Numbers in the image denotes upper number for changes.]  </span><br><br
				<div class="row">
					<!-- <div class="col-md-6"> -->
						<div class="col-md-12 help-image">
							<img src="/images/homepage haru1-01.jpg" alt="Image">
						</div>
						<div class="col-md-12 help-image">
							<img src="/images/homepage haru2-01.jpg" alt="Image">
						</div>
						<div class="col-md-12 help-image">
							<img src="/images/homepage haru3-01.jpg" alt="Image">
						</div>
					</div>
				</div>
			</div>
		</div>



	</div><!--/.col-->

	@endsection