<?php

namespace App\Http\Controllers;

use App\Experience;
use File;
use Illuminate\Http\Request;

class ExperienceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $experiences = experience::all();
        return view ('dashboard.experience.index',compact('experiences'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.experience.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $experiences = new Experience();
        $request->validate([
            'name' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $experiences->name = $request->name;
        if(file_exists($request->file('image'))){
            $image = $request->file('image');
            $iimageName =  "expertise".time().'.'.$request->file('image')->getClientOriginalName();
            $image->move(public_path('uploads'),$iimageName);
            $experiences->image = $iimageName;
        }
        else{
            $experiences->image = 'default-thumbnail.png';
        }        
        $experiences->save();
        return redirect('/backoffice/experience');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function show(Experience $experience)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function edit(Experience $experience,$id)
    {
        $experiences = Experience::findOrFail($id);
        return view ('dashboard.experience.edit',compact('experiences'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Experience $experience,$id)
    {
        $experiences = Experience::findOrFail($id);
        $request->validate([
            'name' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $experiences->name = $request->name;
        if(file_exists($request->file('image'))){
            $image = $request->file('image');
            $iimageName =  "expertise".time().'.'.$request->file('image')->getClientOriginalName();
            $image->move(public_path('uploads'),$iimageName);
            $experiences->image = $iimageName;
        }
        else{
            $experiences->image = $experiences->image;
        }        
        $experiences->save();
        return redirect('/backoffice/experience');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $experiences = Experience::find($id);
        $image_path = public_path("uploads/".$experiences->image);
        if(file_exists($image_path)){
            //File::delete($image_path);
            File::delete( $image_path);
        }
        $experiences->delete();
        return redirect('/backoffice/experience');
    }
}
