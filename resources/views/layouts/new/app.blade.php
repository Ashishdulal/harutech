<!-- Page Title--><!DOCTYPE html>
<html class="wide wow-animation" lang="en" xmlns:fb="http://ogp.me/ns/fb#" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        @isset($title)
        {{ $title }} | {{ $discription }} 
        @endisset
    </title>
@yield('metaDescription')
@yield('customCss')
<script data-ad-client="ca-pub-2231268597887328" async
src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<?php 
    use App\homePage;
    use App\Menu;
    use App\PageSetting;

    $homepages = homePage::findOrFail('1');
    $pageSetting = PageSetting::findOrFail('1');
    ?>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <!-- <script src="{{asset('/cdn-cgi/apps/head/3ts2ksMwXvKRuG480KNifJ2_JNM.js')}}"></script> -->
    <link rel="icon" href="/uploads/homepage/{{$pageSetting->site_favicon}}" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//cdn.materialdesignicons.com/3.8.95/css/materialdesignicons.min.css">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato:300i,400,400i,700%7CMontserrat:400,500,600,700%7CPlayfair+Display:400,700,700i%7COswald:400,700,700i">

    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/modification.css')}}">
    <link rel="stylesheet" href="{{asset('css/fonts.css')}}">
    <link rel="stylesheet" href="{{asset('css/new_style.css')}}">
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
<![endif]-->
</head>
<body>
    @include('layouts.new.header')
    @yield('content')
    @include('layouts.new.footer')

</div>
<!-- Global Mailform Output-->
<div class="snackbars" id="form-output-global"></div>
<!-- Javascript-->
<script src="{{asset('js/core.min.js')}}"></script>
<script src="{{asset('js/script.js')}}"></script>
<style>
    .pb-video-container {
        padding-top: 20px;
        background: #bdc3c7;
        font-family: Lato;
    }

    .pb-video {
        border: 1px solid #e6e6e6;
        padding: 5px;
    }

    .pb-video:hover {
        background: #0f0d35;
    }

    .pb-video-frame {
        transition: width 2s, height 2s;
    }


    .pb-row {
        margin-bottom: 10px;
    }
    .chat-logo-cont {
    z-index: 999;
}
a.chat-logo {
    position: fixed;
    z-index: 999;
    float: right;
    right: 33px;
    font-size: 40px;
    bottom: 34px;
    border-radius: 50%;
    color: #ffff;
    background: #2a81f4;
    padding: 11px 15px;
    transition: .45s all ease-in-out;
}
a.chat-logo:hover {
    color: #fff;
    background: #4FCE5D;
    text-decoration: none;
}
</style>
<!-- coded for jivo chat-->
<!-- <script src="//code.jivosite.com/widget/htA0Y9gWp9" async></script> -->
<div class="chat-logo-cont">
<a <?php echo ($homepages->logo_image)?>></a>
</div>
</body>

</html>