@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Home</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item">
						<a href="/backoffice/users">All Users</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">Edit</li>
				</ul>
			</div>
		</div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
		<div class="card-body card-block">
			<form method="post" action="/backoffice/users/edit/{{ $users->id }}">

				{{csrf_field()}}

				<label for="name" >User Name</label>
				<div  class="form-group">
					<input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-danger': '' }}" value="{{ $users->name }}" placeholder="Enter the name">
				</div>
				<label for="email" >User E-mail</label>
				<div  class="form-group">
					<input type="mail" name="email" class="form-control {{ $errors->has('email') ? 'is-danger': '' }}" value="{{ $users->email }}"" placeholder="Enter the email">
				</div>
				<label for="password" >Password</label>
				<div  class="form-group">
					<input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-danger': '' }}" placeholder="Enter the new password">
				</div>
				<button type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-dot-circle-o"></i> Submit
				</button>
				<button type="reset" class="btn btn-danger btn-sm">
					<i class="fa fa-ban"></i> Reset
				</button>
			</form>
		</div>
		<div class="card-footer">
			
		</div>
	</div>
</div>



</div><!--/.col-->

@endsection