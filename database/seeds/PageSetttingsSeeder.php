<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\PageSetting;

class PageSetttingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = new PageSetting();
        $page->site_title = 'Demo';
        $page->tagline = 'lorem ipsum';
        $page->site_url = 'demosite.com';
        $page->email_address = 'demosite@mail.com';
        $page->site_logo = 'homepage_logo1577271959.kx-logo.png';
        $page->site_favicon = 'title-logo.png';
        $page->footer_text = 'Copyright© 2019 All Rights Reserved by Nepgeeks Technology';
        $page->footer_visibility = '0';
        $page->copyright_text = 'Copyright© 2019 All Rights Reserved by Nepgeeks Technology';
        $page->permalink_seo = 'demosite';
        $page->meta_keywords_seo = 'demosite';
        $page->meta_description_seo = 'demosite';
        $page->save();
    }
}