 @extends('layouts.new.app', ['title' => 'News'],['discription'=> ($pageSetting->tagline)])

 @section('content')
 <style type="text/css">
  .gallery-item{
    height: auto;
  }
</style>
<section class="section-page-title" style="background-image: url(images/banner/{{$blogBanner->blog_banner}}); background-size: cover;">
  <div class="container">
    <h1 class="page-title">News</h1>
  </div>
</section>
<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="/">Home</a></li>
      <li class="active">News</li>
    </ul>
  </div>
</section>
<section class="section section-lg bg-default">
  <div class="container container-responsive">
    <div class="row row-50">
      <div class="col-lg-8">
        <div class="row row-50">
          <div class="col-lg-11">
            @if(count($blogs))
            @foreach($blogs as $blog)
            @if ($blog->cat_id != 3)
            <div class="post-corporate">
              <div class="post-corporate-img"><a href="/news-detail/{{$blog->id}}"><img src="uploads/{{$blog-> f_image}}" alt="{{$blog-> title}}" width="735" height="400"/></a>
                     <!--  <ul class="tag-list"> 
                        <li><a href="#">Beauty</a></li>
                        <li><a href="#">Salon</a></li>
                      </ul> -->
                    </div>
                    <div class="post-corporate-caption">
                      <h4 class="post-corporate-title"><a href="/news-detail/{{$blog->id}}">{{$blog->title}}</a></h4>
                      <div class="blog-prag"><?php echo ($blog -> description)?></div>
                      <ul class="post-corporate-meta-list">
                        <li class="time">{{ $blog->created_at->format('d M , Y') }}</li>
                        <li>by Admin </li>
                      </ul>
                    </div>
                  </div>
                  @endif
                  @endforeach
                  @else
                  <h5>There are no posts in News.</h5>
                  @endif
                </div>
                <div class="col-12">
                  <ul class="pagination">
                    <li class="page-item">{{ $blogs->links() }}</li>
                    <!-- <li class="page-item page-item-control"><a class="page-link" href="#" aria-label="Previous">prev</a></li>
                    <li class="page-item active"><span class="page-link">1</span></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                    <li class="page-item page-item-control"><a class="page-link" href="#" aria-label="Next">next</a></li> -->
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="blog-aside-list">
                <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Categories</h4>
                  <ul class="list-marked">
                    <li><a href="/news">All</a></li>
                    @foreach($blogcategories as $blogcategory)
                    @if ($blogcategory->id !== 3)
                    <li><a href="/news/category/{{$blogcategory->id}}">{{$blogcategory->title}}</a></li>
                    @endif
                    @endforeach
                  </ul>
                </div>
<!--                 <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Search</h4>
                  <form class="rd-search" action="search-results" method="GET" data-search-live="rd-search-results-live">
                    <div class="form-wrap">
                      <label class="form-label" for="rd-search-form-input">Search...</label>
                      <input class="form-input" id="rd-search-form-input" type="text" name="s" autocomplete="off">
                      <button class="icon mdi mdi-magnify" type="submit"></button>
                    </div>
                  </form>
                </div> -->
                <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Portfolio</h4>
                  <ul class="list-marked">
                    <?php $count=1; ?>
                    @foreach($gallery as $galley)
                    <li><a target="_blank" href="{{$galley->url}}">{{$galley->name}}</a></li>
                    <?php $count ++; ?>
                    <?php if($count > 4)
                    break
                    ?>
                    @endforeach
                  </ul>
                </div>
<!--                 <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Portfolio</h4>
                  <div class="blog-aside-gallery" data-lightgallery="group">
                    <?php $count=1; ?>
                    @foreach($gallery as $galley)
                    <div class="blog-aside-gallery-item"><a class="gallery-item" href="{{$galley->url}}" ><img src="/uploads/gallery/{{$galley->file}}" alt="" width="570" height="570"/><span class="gallery-item-title"><i class="fa fa-external-link" aria-hidden="true"></i></span></a></div>
                    <?php $count ++; ?>
                    <?php if($count > 4)
                    break
                    ?>
                    @endforeach
                  </div>
                </div> -->
                <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Recent News</h4>
                  <?php $count=1; ?>
                  @if(count($blogs))
                  @foreach($blogs as $blog)
                  @if ($blog->cat_id != 3)
                  <div class="blog-aside-post">
                    <h5><a href="/news-detail/{{$blog->id}}"> {{$blog->title}}</a></h5>
                    <p>{{ $blog->created_at->format('d M , Y | H:i') }}</p>
                  </div>
                  <?php $count ++; ?>
                  <?php if($count > 5)
                  break
                  ?>
                  @endif
                  @endforeach
                  @else
                  <h5>There are no posts in News.</h5>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Page Footer-->
      @endsection