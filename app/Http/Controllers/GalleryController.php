<?php

namespace App\Http\Controllers;

use App\Jobs_detail;
use App\Gallery;
use File;
use App\Gallerycategory;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallery = Gallery::latest()->paginate(10);
        $galleryBanner = Jobs_detail::findOrFail('1');
        $gallerycategories = Gallerycategory::all();
        return view ('dashboard.gallery.index',compact('gallery','gallerycategories','galleryBanner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gallerycategories = Gallerycategory::latest()->get();
        return view ('dashboard.gallery.multiplegallery',compact('gallerycategories'));
    }

    public function createVideo()
    {
        return view ('dashboard.gallery.createVideo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $gallery = new Gallery();
        $request->validate([
            'name' => 'required',
            'cat_id' => 'required',
            'file' => 'required',
            'file' => 'image|mimes:jpg,png,jpeg|max:5024',
        ]);
        $gallery->name = $request->name;
        $gallery->url = $request->url;
        $gallery->cat_id = $request->cat_id;
        if(file_exists($request->file('file'))){
             $image = $request->file('file');
            $imageName = "gallery".time().'.'.$image->getClientOriginalName();
        $image->move(public_path('uploads/gallery'),$imageName);
        $gallery->file = $imageName;
        }
        else{
            // $gallery->file = 'default-thumbnail.png';
        return redirect('/backoffice/gallery');
        }
   
        $gallery->save();
        
        return redirect('/backoffice/gallery');
    }

    public function storeVideo(Request $request)
    {
        // $gallery = new Gallery();
        // $request->validate([
        //     'name' => 'required',
        //     'cat_id' => 'required',
        //     'file' => 'image|mimes:jpg,png,jpeg|max:5024',
        // ]);
        // $gallery->name = $request->name;
        // $gallery->cat_id = $request->cat_id;
        // if(file_exists($request->file('file'))){
        //     $file = "gallery".time().'.'.$request->file('file')->getclientOriginalExtension();
        //     $location = public_path('uploads/gallery');
        //     $request->file('file')->move($location, $file);
        //     $gallery->file = $file;
        // }
        // else{
        //     $gallery->file = 'default-thumbnail.png';
        // }        
        // $gallery->save();
        // return redirect('/home/gallery');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gallery  $Gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $Gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gallery  $Gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $Gallery,$id)
    {
        $gallery = Gallery::findOrFail($id);
        $gallerycategories = Gallerycategory::latest()->get();
        return view ('dashboard.gallery.edit',compact('gallery','gallerycategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gallery  $Gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gallery $Gallery,$id)
    {
        $gallery = Gallery::findOrFail($id);
        $request->validate([
            'name' => 'required',
            'cat_id' => 'required',
            'file' => 'image|mimes:jpg,png,jpeg|max:5024',
        ]);
        $gallery->url = $request->url;
        $gallery->name = $request->name;
        $gallery->cat_id = $request->cat_id;
        if(file_exists($request->file('file'))){
            $file = "gallery".time().'.'.$request->file('file')->getclientOriginalName();
            $location = public_path('uploads/gallery');
            $request->file('file')->move($location, $file);
            $gallery->file = $file;
        }
        else{
            $gallery->file = $gallery->file;
        }        
        $gallery->save();
        return redirect('/backoffice/gallery');
    }

    public function updateVideo(Request $request, Gallery $Gallery,$id)
    {
        // $gallery = Gallery::findOrFail($id);
        // $request->validate([
        //     'name' => 'required',
        //     'image' => 'mimetypes:video/mp4,video/avi,video/mpeg,video/mkv,qt | max:500000 ',
        // ]);
        // $gallery->name = $request->name;
        // if(file_exists($request->file('image'))){
        //     $image = "gallery".time().'.'.$request->file('image')->getclientOriginalExtension();
        //     $location = public_path('uploads/gallery');
        //     $request->file('image')->move($location, $image);
        //     $gallery->image = $image;
        // }
        // else{
        //     $gallery->image = $gallery->image;
        // }        
        // $gallery->save();
        // return redirect('/home/gallery');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gallery  $Gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy( Gallery $Gallery,$id)
    {
        $gallery = Gallery::findOrFail($id);
        $image_path = public_path("uploads/gallery/".$gallery->file);
        if(file_exists($image_path)){
            //File::delete($image_path);
            File::delete( $image_path);
        }
        $gallery->delete();
        return redirect()->back();
    }
}
