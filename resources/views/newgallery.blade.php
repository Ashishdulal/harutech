 @extends('layouts.new.app', ['title' => 'Portfolio'],['discription'=> ($pageSetting->tagline)])

 @section('content')
 <section class="section-page-title" style="background-image: url(images/banner/{{$portfolioBanner->service_banner}}); background-size: cover;">
  <div class="container">
    <h1 class="page-title">Portfolio</h1>
  </div>
</section>
<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="/">Home</a></li>
      <li class="active">Portfolio</li>
    </ul>
  </div>
</section>
<section class="section section-lg bg-default text-center">
  <div class="container">
    <h2>All Portfolio</h2>
    <div class="divider-lg"></div>
    <?php echo ($homepages->portfolio_content)?>
    <div class="row row-30">
      <!-- Isotope Filters-->
      <div class="col-lg-12">
        <div class="isotope-filters isotope-filters-horizontal">
          <button class="isotope-filters-toggle button button-sm button-primary" data-custom-toggle="#isotope-filters" data-custom-toggle-disable-on-blur="true">Filter<span class="caret"></span></button>
          <ul class="isotope-filters-list" id="isotope-filters">
            <li><a class="active" data-isotope-filter="*" data-isotope-group="gallery" href="#">All</a></li>
            @foreach($gallerycategories as $gallerycat)
            <li><a data-isotope-filter="{{$gallerycat->id}}" data-isotope-group="gallery" href="#">{{$gallerycat->title}} </a></li>
            @endforeach
          </ul>
        </div>
      </div>
      </div>
    </div>
      <!-- Isotope Content-->
      <div class="container-fluid container-responsive">
      <div class="col-lg-12">
        <div class="isotope row" data-isotope-layout="masonry" data-isotope-group="gallery" data-lightgallery="group" data-column-class=".col-sm-6.col-lg-4">
          @foreach($gallerycategories as $gallcat)
          @foreach ($gallery as $galle)
          @if($gallcat->id == $galle->cat_id)
          <div class="col-sm-6 col-lg-4 isotope-item" data-filter="{{$galle->cat_id}}"><a class="gallery-item"  href="{{$galle->url}}"><img class="port-img" src="/uploads/gallery/{{$galle->file}}" alt="" width="570" height="570"/><span class="gallery-item-title">
            <div class="form-button1"><i class="fa fa-external-link" aria-hidden="true"></i><!-- <button class="button button-primary" type="submit">Demo</button> --></div></span><!-- <span class="gallery-item-button"></span> --></a>
            <span style="text-transform: capitalize;" class="gallery-item-title">{{$galle->name}}</span>
            
          </div>
          @endif
          @endforeach
          @endforeach
        </div>
      </div>
      </div>
      <div class="col-12">
        <ul class="pagination">
          <li style="margin: 0 auto;" class="page-item"></li>
        </ul>
      
  </div>
</section>
        <section class="section-transform-bottom">
          <div class="container-fluid section-md bg-primary context-dark">
            <div style="margin-right: 0px;" class="row justify-content-center row-50">
              <div class="col-sm-10 text-center">
                <h2>Subscribe to Our Newsletter</h2>
                <div class="divider-lg"></div>
              </div>
              <div class="col-sm-10 col-lg-6">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}<br></li>
                    @endforeach
                  </ul>
                </div>
                @endif
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
                </div>
                @endif
                <!-- RD Mailform-->
                <form class="rd-form-inline" method="post" action="{{url('/subscribe/send')}}">
                  @csrf
                  <div class="form-wrap">
                    <input class="form-input" id="subscribe-form-0-email" type="email" name="email" required="" />
                    <label class="form-label" for="subscribe-form-0-email">Your E-mail</label>
                  </div>
                  <div class="form-button1">
                    <button class="button button-primary" type="submit">Subscribe</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
<!-- Page Footer-->
@endsection