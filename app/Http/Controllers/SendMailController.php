<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\PageSetting;
use App\Newsletter;
use App\Rules\Duplicateemail;


class SendMailController extends Controller
{
  function send(Request $request)
  {
    $pageSetting = PageSetting::findOrFail('1');
    $this->validate($request, [
      'name'     =>  'required',
      'sec_name'     =>  'required',
      'email'  =>  'required|email',
      'phone' =>  'required',
      'subject' =>  'required',
      'message' =>  'required'
    ]);

    $data = array(
      'name'      =>  $request->name,
      'sec_name'      =>  $request->sec_name,
      'email'   =>   $request->email,
      'phone'   =>   $request->phone,
      'subject'   =>   $request->subject,
      'message'   =>   $request->message
    );

    $txt1 = '<html>
    <head>  
    </head>
    <body>
    <p>Hi, This is '. $data['name'] .' '. $data['sec_name'] .'.</p>
    <p>Email:'. $data['email'] .'<br><br>Phone:'. $data['phone'] .'</p>
    <p>Subject:<br>'. $data['subject'] .'</p>
    <p>Message:<br>'. $data['message'] .'</p>
    <p>It would be appriciative, if i receive the reply soon.</p>
    </body>
    </html>';       

    $to = $pageSetting->email_address;
    $subject = "Inquiry";

    $headers = "From:" . $pageSetting->site_url . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";

    $result=   mail($to,$subject,$txt1,$headers);
    $result=   mail($data['email'],$subject,$txt1,$headers);
    return back()->with('success','Thanks for contacting us!');
  }
  function sendJob(Request $request)
  {
    $pageSetting = PageSetting::findOrFail('1');
    $this->validate($request, [
      'name'     =>  'required',
      'email'  =>  'required|email',
      'phone' =>  'required',
      'message' =>  'required',
      'service' => 'required'
    ]);

    $data = array(
      'name'      =>  $request->name,
      'email'   =>   $request->email,
      'phone'   =>   $request->phone,
      'message'   =>   $request->message,
      'service'   =>   $request->service
    );

    $txt1 = '<html>
    <head>  
    </head>
    <body>
    <p>Hi, This is '. $data['name'] .'.</p>
    <p>Email:'. $data['email'] .'<br><br>Phone:'. $data['phone'] .'</p>
    <p>Job Needed:'. $data['service'] .'</p>
    <p>Message:<br>'. $data['message'] .'</p>
    <p>It would be appriciative, if i receive the reply soon.</p>
    </body>
    </html>';       

    $to = $pageSetting->email_address;
    $subject = "Inquiry";

    $headers = "From:" . $pageSetting->site_url . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";

    $result=   mail($to,$subject,$txt1,$headers);
    $result=   mail($data['email'],$subject,$txt1,$headers);
    return back()->with('success','Thanks for contacting us. We will reach out to you soon!');
  }

  function subscribe(Request $request)
  {
    $pageSetting = PageSetting::findOrFail('1');
    $this->validate($request, [
      'email'  =>  ['required', 'email', new Duplicateemail],
    ]);

    $sdata = array(
      'email'   =>   $request->email
    );

    $txt2 = '<html>
    <head>  
    </head>
    <body>
    <p>Hi, This is '. $sdata['email'] .'</p>
    <p>I want to subscribe for the newsletter.</p>
    <p>It would be appriciative, if i receive the updates.</p>
    </body>
    </html>';       
    Newsletter::create($request->all());
    $to = $pageSetting->email_address;
    $subject = "Subscription Inquiry";

    $headers = "From:" . $pageSetting->site_url . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";

    $result=   mail($to,$subject,$txt2,$headers);
    return back()->with('success','You have successfully applied for our Newsletter!');
  }

  function newsletter(Request $request){
    $pageSetting = PageSetting::findOrFail('1');
    $ndata = array(
      'description'   =>   $request->description,
      'title'   =>   $request->title,
      'image'   =>   $request->image,
      'id'   =>   $request->id,

    );

    $txt3 = '<html>
    <head>  
    </head>
    <body>
    <table>
    <tbody>
    <td class="m_5772815688879916528tableWhiteBackground" style="border-collapse:collapse" width="100%" bgcolor="#e6e7e8" valign="top" align="center">
    <table class="m_5772815688879916528table" width="600" border="0" cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
    <td class="m_5772815688879916528table" style="border-collapse:collapse;padding:30px 0" align="center" valign="top" bgcolor="#2a81f4">
    <table class="m_5772815688879916528table90" width="450" border="0" cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
    <td class="m_5772815688879916528table" style="border-collapse:collapse" width="450" height="3" align="center" valign="top" bgcolor="#2a81f4">
    <a href="http://mysedap.net/blog-detail/'. $ndata['id'] .'" target="_blank" style="line-height:39px;text-transform: capitalize; text-decoration:none;color:#ffffff;font-size:35px;font-weight:bold;font-family:helvetica,arial,sans-serif">'. $ndata['title'] .'</a>
    </td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    </tbody>
    </table>
    <table class="m_5772815688879916528hide" width="600" border="0" cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
    <td style="border-collapse:collapse;padding:30px 0;padding-bottom:10px" height="2" align="center" width="600" bgcolor="#fff" valign="bottom">
    <a href="http://mysedap.net/blog-detail/'. $ndata['id'] .'" target="_blank"><img class="m_5772815688879916528table CToWUd" style="color:#ffffff;outline:none;text-decoration:none;display:block" src="http://mysedap.net/uploads/'. $ndata['image'] .'" width="185" border="0" alt="'. $ndata['title'] .'">
    </a>
    </td>
    </tr>
    </tbody>
    </table>

    <table class="m_5772815688879916528table" width="600" border="0" cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
    <td class="m_5772815688879916528table" style="border-collapse:collapse;padding:40px 0" align="center" valign="top" bgcolor="#2a81f4">
    <table class="m_5772815688879916528table90" width="80%" border="0" cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
    <td class="m_5772815688879916528table" style="border-collapse:collapse" width="600" height="3" align="center" valign="top" bgcolor="#2a81f4">
    <font style="text-decoration:none;color:#ffffff;font-size:16px;font-weight:400;line-height:20px;font-family:helvetica,arial,sans-serif">
    <p>'. $ndata['description'] .'</p>
    <a href="http://mysedap.net/blog-detail/'. $ndata['id'] .'" class="btn btn-primary" style="text-decoration:none;margin-top:10px;color:#ffdf88;font-size:12px;font-weight:bold;font-family:helvetica,arial,sans-serif" target="_blank">READ MORE ⟩</a>
    </font>
    </td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    </tbody>
    </table>
    </td>
    </table>
    </tbody>
    </body>
    </html>';

    $allMails = Newsletter::all();
    foreach ($allMails as $mail) {
      $to = $mail->email;

      $subject = "Newsletter";

      $headers = "From:" . $pageSetting->site_url . "\r\n";
      $headers .= "MIME-Version: 1.0\r\n";
      $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";
      $result=   mail($to,$subject,$txt3,$headers);
    }
    return back()->with('success','You have successfully sent the Newsletter!');
  }

}
