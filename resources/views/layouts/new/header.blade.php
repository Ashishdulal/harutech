
    <div class="page">
      <!-- Page Header-->
      <header class="section page-header">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-classic rd-navbar-classic-minimal" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-lg-stick-up-offset="46px" data-xl-stick-up-offset="46px" data-xxl-stick-up-offset="46px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
            <div class="rd-navbar-aside-outer rd-navbar-collapse toggle-original-elements">
              <div class="rd-navbar-aside">
                <div class="header-info">
                  <ul class="list-inline list-inline-md">
                    <li>
                      <div class="unit unit-spacing-xs align-items-center">
                        <div class="unit-left font-weight-bold">Free Call:</div>
                        <div class="unit-body"><a href="tel:{{$homepages->phone_number}}">{{$homepages->phone_number}}</a></div>
                      </div>
                    </li>
                    <li>
                      <div class="unit unit-spacing-xs align-items-center">
                        <div class="unit-left font-weight-bold">Opening Hours: </div>
                        <div class="unit-body"> {{$homepages->opening_hours}}</div>
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="social-block">
                  <ul class="list-inline">
                    <li><a class="icon fa-facebook" target="_blank" href="{{$homepages->Social_icon_fb}}"></a></li>
                    <li><a class="icon fa-twitter" target="_blank" href="{{$homepages->Social_icon_twitter}}"></a></li>
                    <li><a class="icon fa-instagram" target="_blank" href="{{$homepages->Social_icon_insta}}"></a></li>
                    <li><a class="icon fa-linkedin" target="_blank" href="{{$homepages->Social_icon_linkedin}}"></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="rd-navbar-main-outer">
              <div class="rd-navbar-main">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">
                  <!-- RD Navbar Toggle-->
                  <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                  <!-- RD Navbar Brand-->
                  <div class="rd-navbar-brand"><a class="brand" href="/"><img src="/uploads/homepage/{{$pageSetting->site_logo}}" alt="Logo" width="158" height="58"/></a></div>
                  <!-- <span><a class="rd-nav-link" href="/"><h2 class="text-primary">HARUTECH</h2></a></span> -->
                </div>
                <div class="rd-navbar-main-element">
                  <div class="rd-navbar-nav-wrap">
                    <!-- RD Navbar Nav-->
                    <ul class="rd-navbar-nav">
                      <?php 
                      use App\Menu;
                      $primarymenus = Menu::where('parent_id', null)->orderBy('order', 'ASC')->get();
                        foreach($primarymenus as $pmenu){
                            ?>

                      <li class="rd-nav-item {{ request()->is($pmenu->url) ? 'active' : ''}} {{ Request::segment(1)== str_replace('/', '',$pmenu->url) ? 'active' : '' }} ">
                        <a class="rd-nav-link" href="{{ $pmenu->url }}">{{$pmenu->label}}</a>
                        <div class="divider-menu {{ request()->is($pmenu->url) ? 'active' : ''}} {{ Request::segment(1)== str_replace('/', '',$pmenu->url) ? 'active' : '' }}"></div>
                            <?php 
                            $secondarymenus = Menu::where('parent_id', $pmenu->id)->get();
                            if(count($secondarymenus)){?>
                                <ul class="rd-menu rd-navbar-dropdown">
                            <?php foreach($secondarymenus as $secm){ ?>
                                  <li class="rd-dropdown-item">
                                    <a class="rd-dropdown-link" href="{{ $secm->url }}">{{ $secm->label }}</a>
                                  </li>  
                          <?php } ?>
                                </ul>                                 
                          <?php } ?>
                      </li>
                    <?php      
                        }
                    ?>
                      </ul>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>
      <!-- The Modal -->
<div class="modal fade" id="myModal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content make-other">

      <!-- Modal Header -->
      <div class="modal-header my-modal">
        <div class="intro_form_title">Make an Appointment</div>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="col-lg-12 intro_col hov_form">
        @if (count($errors) > 0)
    <div class="alert alert-danger">
     <button type="button" class="close" data-dismiss="alert">×</button>
     <ul>
      @foreach ($errors->all() as $error)
       <li>{{ $error }}</li>
      @endforeach
     </ul>
    </div>
   @endif
   @if ($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
        <form style="padding: 20px 0;" class="text-left" action="{{url('sendemail/send')}}" method="post" >
        @csrf
        <div><p>You can contact us any way that is convenient for you. We are available 24/7 via fax or email.
You can also use a quick contact form below or visit us personally.</p></div>
        <div class="row row-15">
          <div class="col-sm-6">
            <div class="form-wrap">
              <label class="form-label" for="contact-name">First name</label>
              <input class="form-input" id="contact-name" type="text" name="name" required="">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-wrap">
              <label class="form-label" for="contact-sec-name">Last name</label>
              <input class="form-input" id="contact-sec-name" type="text" name="sec_name" required="">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-wrap">
              <label class="form-label" for="contact-phone">Phone</label>
              <input class="form-input" id="contact-phone" type="text" name="phone" required="">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-wrap">
              <label class="form-label" for="contact-email">E-Mail</label>
              <input class="form-input" id="contact-email" type="email" name="email" required="">
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-wrap">
              <label class="form-label" for="contact-subject">Subject</label>
              <input class="form-input" id="contact-subject" type="text" name="subject" required="">
            </div>
          </div>
          <div class="col-12">
            <div class="form-wrap">
              <label class="form-label" for="contact-message">Message</label>
              <textarea class="form-input" id="contact-message" name="message" required=""></textarea>
            </div>
          </div>
        </div>
        <div class="form-button group-sm text-left">
          <button class="button button-primary" type="submit">Send message</button>
        </div>
      </form>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>