 @extends('layouts.new.app', ['title' => 'Careers'],['discription'=> ($pageSetting->tagline)])

 @section('content')
 <section class="section-page-title" style="background-image: url(images/banner/{{$jobBanner->jobs_banner}}); background-size: cover;">
 	<div class="container">
 		<h1 class="page-title">Careers</h1>
 	</div>
 </section>
 <section class="breadcrumbs-custom">
 	<div class="container">
 		<ul class="breadcrumbs-custom-path">
 			<li><a href="/">Home</a></li>
 			<li class="active">Careers</li>
 		</ul>
 	</div>
 </section>
 <section class="section section-sm bg-gray-100 text-center">
 	<div class="container-fluid container-responsive">
 		<h2>Our Careers</h2>
 		<div class="divider-lg"></div>
 		<div class="row justify-content-center">
 			<div class="col-md-10 col-lg-9">
 				<?php echo ($jobBanner->jobs_detail) ?>
 			</div>
 		</div>
 		<div class="row icon-modern-list no-gutters">
 			@foreach($jobs as $service)
 			<div class="col-sm-6 col-lg-4">
 				<article class="box-icon-modern modern-variant-2">
 					<div class="icon-modern">
 						<span> <aa data-toggle="modal" data-target="#services{{$service->id}}" href="#"> <img style="cursor:pointer;" aria-expanded="true" src="/uploads/{{$service->image}}"></a></span>
 						</div>
 						<h4 class="box-icon-modern-title"><a data-toggle="modal" data-target="#services{{$service->id}}" href="#">{{$service->name}}</a></h4>
 						<div class="divider"></div>
 						<!-- <p><?php echo ($service->description)?></p> -->
 					</article>
 				</div>
 				<!-- Modal -->
 				<div class="modal fade" id="services{{$service->id}}" role="dialog">
 					<div class="modal-dialog modal-lg">
 						
 						<!-- Modal content-->
 						<div class="modal-content">
 							<div class="modal-header">
 								<h4 class="modal-title">Our Careers</h4>
 								<button type="button" class="close" data-dismiss="modal">&times;</button>
 							</div>
 							<div class="modal-body">
 								<article class="modern-variant-2">
 									<div class="icon-modern">
 										<span><img style="max-width: 50%;" aria-expanded="true" aria-controls="services{{$service->id}}" style="" src="/uploads/{{$service->image}}"></span>
 									</div>
 									<h4 style="margin-bottom: 10px;" class="box-icon-modern-title">{{$service->name}}</h4>
 									<div class="divider"></div>
 									<p><?php echo ($service->description)?></p>
 								</article>
 								<div class="form-button">
 									<button style="border-color: #0052cc;" class="button button-primary" data-toggle="modal" data-target="#myModal1" href="#" type="submit">Apply Now</button>
 								</div>
 							</div>
 							<div class="modal-footer">
 								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 							</div>
 						</div>
 						
 					</div>
 				</div>
 				@endforeach
 			</div>
 		</div>
 	</section>
 	<section class="section-transform-bottom">
 		<div class="container-fluid section-sm bg-primary context-dark">
 			<div style="margin-right: 0px;" class="row justify-content-center row-30">
 				<div class="col-sm-10 text-center">
 					<h2>Join us</h2>
 				<div class="divider-lg"></div>
 				</div>
 				<div class="col-sm-10 col-lg-6 text-center">
 					<p class="block-lg">Please submit your application.</p>
 					<div class="form-button">
 						<button class="button button-primary" data-toggle="modal" data-target="#myModal1" href="#" type="submit">APPLY NOW</button>
 					</div>
 				</div>
 			</div>
 		</div>
 	</section>
 	<!-- Page Footer-->

 	<!-- The Modal -->
 	<div class="modal fade" id="myModal1">
 		<div class="modal-dialog modal-dialog-centered">
 			<div class="modal-content make-other">

 				<!-- Modal Header -->
 				<div class="modal-header my-modal">
 					<div class="intro_form_title">Join Us</div>
 					<button type="button" class="close" data-dismiss="modal">&times;</button>
 				</div>
 				<div class="col-lg-12 intro_col hov_form">
 					@if (count($errors) > 0)
 					<div class="alert alert-danger">
 						<button type="button" class="close" data-dismiss="alert">×</button>
 						<ul>
 							@foreach ($errors->all() as $error)
 							<li>{{ $error }}</li>
 							@endforeach
 						</ul>
 					</div>
 					@endif
 					@if ($message = Session::get('success'))
 					<div class="alert alert-success alert-block">
 						<button type="button" class="close" data-dismiss="alert">×</button>
 						<strong>{{ $message }}</strong>
 					</div>
 					@endif
 					<form style="padding: 20px 0;" class="text-left" action="{{url('sendjob/send')}}" method="post" >
 						@csrf
 						<div><p>Please submit your application.</p></div>
 						<div class="row row-15">
 							<div class="col-sm-6">
 								<div class="form-wrap">
 									<input class="form-input" id="contact-name" type="text" name="name" required="" placeholder="Name">
 								</div>
 							</div>
 							<div class="col-sm-6">
 								<div class="form-wrap">
 									<input class="form-input" id="contact-email" type="email" name="email" required="" placeholder="E-Mail">
 								</div>
 							</div>
 							<div class="col-sm-12">
 								<div class="form-wrap">
 									<input class="form-input" id="contact-phone" type="text" name="phone" required="" placeholder="Phone">
 								</div>
 							</div>
 							<div class="col-sm-12">
 								<div class="">
 									<select name="service" class="form-input" required>
 										<option disabled="" selected="">Select the Career</option>
 										@foreach($alljobs as $job)
 										<option value="{{ $job->name }}">{{$job->name}}</option>
 										@endforeach
 									</select>
 								</div>
 							</div>
 							<div class="col-12">
 								<div class="form-wrap">
 									<textarea class="form-input" id="contact-message" name="message" required="" placeholder="Message"></textarea>
 								</div>
 							</div>
 						</div>
 						<div class="form-button group-sm text-left">
 							<button type="submit" class="button button-primary bg-primary contact_button trans_200">Apply Now</button>
 						</div>
 					</form>
 				</div>
 				<!-- Modal footer -->
 				<div class="modal-footer">
 					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
 				</div>

 			</div>
 		</div>
 	</div>
 	@endsection