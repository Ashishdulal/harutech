<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutUsPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_us_pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('banner_image');
            $table->text('who_are_we');
            $table->text('who_are_we_image1');
            $table->text('who_are_we_image2');
            $table->text('video_title');
            $table->text('video_image')->nullable();
            $table->text('video');
            $table->text('team_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_us_pages');
    }
}
