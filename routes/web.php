<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/backoffice', 'HomeController@index')->name('home');

Route::get('menu','MainPageController@index')->name('menu.get');

Route::fallback(function() {
    return view('nopage');
});

Route::get('/','MainPageController@index');
Route::get('/landing','MainPageController@landingPage');
Route::get('/portfolio','MainPageController@galleryPage');
Route::get('/about-us','MainPageController@aboutPage');
Route::get('/services','MainPageController@servicePage');
Route::get('/news','MainPageController@blogIndex');
Route::get('/news/category/{id}','MainPageController@blogCategoryPage');
Route::get('/news-detail/{id}','MainPageController@blogDetail')->name('post.show');
Route::get('/contacts','MainPageController@contact');
Route::get('/training','MainPageController@trainingPage');

Route::post('/sendemail/send', 'SendMailController@send');
Route::post('/sendjob/send', 'SendMailController@sendJob');
Route::post('/subscribe/send', 'SendMailController@subscribe');
Route::post('/backoffice/newsletter', 'SendMailController@newsletter');

Route::get('/backoffice/subscribers', 'NewsletterController@newsletterIndex');
Route::get('/backoffice/subscriber/destroy/{id}', 'NewsletterController@newsletterDestroy');

Route::get('/backoffice/newsletter', 'NewsletterController@index');

Route::get('/backoffice/users', 'UserPasswordController@index');
Route::get('/backoffice/users/create', 'UserPasswordController@create');
Route::post('/backoffice/users/create', 'UserPasswordController@store')->name('make.user');
Route::get('/backoffice/users/{id}', 'UserPasswordController@show');
Route::get('/backoffice/users/edit/{id}', 'UserPasswordController@edit');
Route::post('/backoffice/users/image/{id}', 'UserPasswordController@imageUpdate');
Route::post('/backoffice/users/edit/{id}', 'UserPasswordController@update')->name('update.user');
Route::get('/backoffice/users/destroy/{id}', 'UserPasswordController@destroy')->name('delete.user');

Route::get('/backoffice/services', 'ServiceController@index');
Route::get('/backoffice/services/create', 'ServiceController@create');
Route::post('/backoffice/services/create', 'ServiceController@store');
Route::get('/backoffice/services/edit/{id}', 'ServiceController@edit')->name('service.edit');
Route::post('/backoffice/services/edit/{id}', 'ServiceController@update')->name('service.update');
Route::delete('/backoffice/services/destroy/{id}', 'ServiceController@destroy')->name('service.delete');

Route::get('/backoffice/gallery-category', 'GallerycategoryController@index');
Route::get('/backoffice/gallery-category/create', 'GallerycategoryController@create');
Route::post('/backoffice/gallery-category/create', 'GallerycategoryController@store')->name('make.gallery');
Route::get('/backoffice/gallery-category/{id}', 'GallerycategoryController@show');
Route::get('/backoffice/gallery-category/edit/{id}', 'GallerycategoryController@edit');
Route::post('/backoffice/gallery-category/edit/{id}', 'GallerycategoryController@update')->name('update.gallery');
Route::get('/backoffice/gallery-category/destroy/{id}', 'GallerycategoryController@destroy')->name('delete.gallery');


Route::get('/backoffice/gallery', 'GalleryController@index');
Route::get('/backoffice/gallery/create', 'GalleryController@create');
Route::post('/backoffice/gallery/create', 'GalleryController@store');
// Route::get('/backoffice/gallery/createVideo', 'GalleryController@createVideo');
// Route::post('/backoffice/gallery/createVideo', 'GalleryController@storeVideo');
Route::get('/backoffice/gallery/edit/{id}', 'GalleryController@edit')->name('gallery.edit');
Route::post('/backoffice/gallery/edit/{id}', 'GalleryController@update')->name('gallery.update');
Route::delete('/backoffice/gallery/destroy/{id}', 'GalleryController@destroy')->name('gallery.delete');


Route::get('/backoffice/news-category', 'BlogcategoryController@index');
Route::get('/backoffice/news-category/create', 'BlogcategoryController@create');
Route::post('/backoffice/news-category/create', 'BlogcategoryController@store')->name('make.blog');
Route::get('/backoffice/news-category/{id}', 'BlogcategoryController@show');
Route::get('/backoffice/news-category/edit/{id}', 'BlogcategoryController@edit');
Route::post('/backoffice/news-category/edit/{id}', 'BlogcategoryController@update')->name('update.blog');
Route::get('/backoffice/news-category/destroy/{id}', 'BlogcategoryController@destroy')->name('delete.blog');


Route::get('/backoffice/news', 'BlogController@index');
Route::get('/backoffice/news/create', 'BlogController@create');
Route::post('/backoffice/news/create', 'BlogController@store')->name('make-post.blog');
Route::get('/backoffice/news/{id}', 'BlogController@show');
Route::get('/backoffice/news/edit/{id}', 'BlogController@edit');
Route::post('/backoffice/news/edit/{id}', 'BlogController@update')->name('update-post.blog');
Route::get('/backoffice/news/destroy/{id}', 'BlogController@destroy')->name('delete-post.blog');


Route::get('/backoffice/sliders', 'SliderController@index');
Route::get('/backoffice/slider/create', 'SliderController@create');
Route::post('/backoffice/slider/create', 'SliderController@store');
Route::get('/backoffice/slider/edit/{id}', 'SliderController@edit')->name('slider.edit');
Route::post('/backoffice/slider/edit/{id}', 'SliderController@update')->name('slider.update');
Route::delete('/backoffice/slider/destroy/{id}', 'SliderController@destroy')->name('slider.delete');


Route::get('/backoffice/testimonials', 'TestimonialController@index');
Route::get('/backoffice/testimonials/create', 'TestimonialController@create');
Route::post('/backoffice/testimonials/create', 'TestimonialController@store');
Route::get('/backoffice/testimonials/edit/{id}', 'TestimonialController@edit')->name('testimonial.edit');
Route::post('/backoffice/testimonials/edit/{id}', 'TestimonialController@update')->name('testimonial.update');
Route::delete('/backoffice/testimonials/destroy/{id}', 'TestimonialController@destroy')->name('testimonial.delete');

Route::get('/backoffice/staffs', 'StaffController@index');
Route::get('/backoffice/staffs/create', 'StaffController@create');
Route::post('/backoffice/staffs/create', 'StaffController@store')->name('make.staff');
Route::get('/backoffice/staffs/{id}', 'StaffController@show');
Route::get('/backoffice/staffs/edit/{id}', 'StaffController@edit');
Route::post('/backoffice/staffs/edit/{id}', 'StaffController@update')->name('update.staff');
Route::delete('/backoffice/staffs/destroy/{id}', 'StaffController@destroy')->name('delete.staff');

Route::get('/backoffice/experience', 'ExperienceController@index');
Route::get('/backoffice/experience/create', 'ExperienceController@create');
Route::post('/backoffice/experience/create', 'ExperienceController@store');
Route::get('/backoffice/experience/edit/{id}', 'ExperienceController@edit')->name('experience.edit');
Route::post('/backoffice/experience/edit/{id}', 'ExperienceController@update')->name('experience.update');
Route::delete('/backoffice/experience/destroy/{id}', 'ExperienceController@destroy')->name('experience.delete');

Route::get('/backoffice/careers', 'JobController@index');
Route::get('/backoffice/careers/create', 'JobController@create');
Route::post('/backoffice/careers/create', 'JobController@store');
Route::get('/backoffice/careers/edit/{id}', 'JobController@edit')->name('job.edit');
Route::post('/backoffice/careers/edit/{id}', 'JobController@update')->name('job.update');
Route::delete('/backoffice/careers/destroy/{id}', 'JobController@destroy')->name('job.delete');

Route::post('/backoffice/banner/image/{id}', 'JobController@bannerImage');

Route::get('/careers', 'MainPageController@jobsIndex');
Route::get('/careers-detail/{id}', 'MainPageController@jobsDetail');

Route::get('/backoffice/homepage', 'HomePageController@edit');
// Route::get('/backoffice/homepage/edit/{id}', 'HomePageController@edit')->name('homepage.edit');
Route::post('/backoffice/homepage/edit/{id}', 'HomePageController@update')->name('homepage.update');

Route::get('/backoffice/about-us', 'AboutUsPageController@edit');
// Route::get('/backoffice/about-us/edit/{id}', 'AboutUsPageController@edit')->name('about-us.edit');
Route::post('/backoffice/about-us/edit/{id}', 'AboutUsPageController@update')->name('about-us.update');

Route::get('/backoffice/contact', 'ContactDetailsController@edit');
// Route::get('/backoffice/contact/edit/{id}', 'ContactDetailsController@edit')->name('contact.edit');
Route::post('/backoffice/contact/edit/{id}', 'ContactDetailsController@update')->name('contact.update');

Route::get('/backoffice/page-setting', 'PageSettingController@index');
Route::post('/backoffice/page-setting/edit/{id}', 'PageSettingController@update')->name('page-setting.update');

Route::get('/backoffice/menus', 'MenuController@index');
Route::get('/backoffice/menu/create', 'MenuController@create');
Route::post('/backoffice/menu/create', 'MenuController@store')->name('make.menu');
Route::get('/backoffice/menu/{id}', 'MenuController@show');
Route::get('/backoffice/menu/edit/{id}', 'MenuController@edit');
Route::post('/backoffice/menu/edit/{id}', 'MenuController@update')->name('update.menu');
Route::get('/backoffice/menu/destroy/{id}', 'MenuController@destroy')->name('delete.menu');