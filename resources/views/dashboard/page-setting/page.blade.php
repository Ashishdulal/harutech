@extends('layouts.dashboard.app')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<style type="text/css">
	.navbar-sidebar2 .navbar__list li .arrow {
		top: auto !important;
		height: 20px !important;
	}
	.btn-default:hover, .btn-default:focus, .btn-default:active, .btn-default.active, .open>.dropdown-toggle.btn-default {
		color: #333;
		background-color: #e6e6e6;
		border-color: #adadad;
	}
	.btn-default {
		color: #333;
		background-color: #fff;
		border-color: #ccc !important;
	}
	.toggle-handle {
		position: relative;
		margin: 0 auto;
		padding-top: 0px;
		padding-bottom: 0px;
		height: 100%;
		width: 0px;
		border-width: 0 1px;
	}
	.nav-tabs>li {
		float: left;
		margin-bottom: -1px;
	}

	.nav>li {
		position: relative;
		display: block;
	}
	.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
		color: #555;
		cursor: default;
		background-color: #fff;
		border: 1px solid #ddd;
		border-bottom-color: transparent;
	}
	.nav-tabs>li>a {
		margin-right: 2px;
		line-height: 1.42857143;
		border: 1px solid transparent;
		border-radius: 4px 4px 0 0;
	}
	.nav>li>a {
		position: relative;
		display: block;
		padding: 10px 15px;
	}
	a:hover, a {
		-webkit-transition: all 0.3s ease;
		-o-transition: all 0.3s ease;
		-moz-transition: all 0.3s ease;
		transition: all 0.3s ease;
	}
	img#blah {
		height: auto !important;
		width: auto !important;
	}
</style>
@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Dashboard</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">page Settings</li>
				</ul>
			</div>
		</div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
		@if($pages)
		<form action="/backoffice/page-setting/edit/{{$pages->id}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
			@csrf
			<div style="text-align: right;margin: 20px 40px 0 0px;" class="home-btn">
				<button type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-dot-circle-o"></i> Update
				</button>
			</div>
			<div class="card-body card-block">
				<div class="container-fluid">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#home">General</a></li>
						<li><a data-toggle="tab" href="#menu1">Footer</a></li>
						<li><a data-toggle="tab" href="#menu2">SEO</a></li>
						<li></li>
					</ul>
					<div class="tab-content">
						<div id="home" class="tab-pane fade active in show">
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="site_title" class=" form-control-label">Site Title</label>
								</div>
								<div class="col-12 col-md-9">
									<input type="text" name="site_title"  rows="9"  class="form-control" value="{{$pages->site_title}}" placeholder="site_title"><br>
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="tagline" class=" form-control-label">Tagline</label>
								</div>
								<div class="col-12 col-md-9">
									<input type="text" name="tagline"  rows="9"  class="form-control" value="{{$pages->tagline}}" placeholder="tagline"><br>
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="site_url" class=" form-control-label">Site Url</label>
								</div>
								<div class="col-12 col-md-9">
									<input type="text" name="site_url"  rows="9"  class="form-control" value="{{$pages->site_url}}" placeholder="site url"><br>
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="email_address" class=" form-control-label">Email Address</label><br>
									<span class="au-breadcrumb-span">[ Note: This will be the email address to recieve all the mails sent from the website. ]</span>
								</div>
								<div class="col-12 col-md-9">
									<input type="text" name="email_address"  rows="9"  class="form-control" value="{{$pages->email_address}}" placeholder="email address"><br>
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="site_logo" class=" form-control-label">Site Logo</label><br>
									<span class="au-breadcrumb-span">[ Note: Please upload the small image size of '.png' type & should be less than 100kb ]</span>
								</div>
								<div class="col-12 col-md-9 process">
									<input onchange="readURL(this);" type="file" id="site_logo" accept="image/png, image/jpg, image/jpeg" name="site_logo" class="form-control-file"><br>
									<img id="blah" src="/uploads/homepage/{{$pages->site_logo}}" alt="site Logo">
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="site_favicon" class=" form-control-label">Favicon</label><br>
									<span class="au-breadcrumb-span">[ Note: Please upload the small image size 20*20 of '.png' type & should be less than 10kb ]</span>
								</div>
								<div class="col-12 col-md-9">
									<input onchange="readURLFav(this);" type="file" id="site_favicon" accept="image/png, image/jpg, image/jpeg" name="site_favicon" class="form-control-file"><br>
									<img id="favBlah" src="/uploads/homepage/{{$pages->site_favicon}}" alt="site Favicon">
								</div>
							</div>
						</div>
						<div id="menu1" class="tab-pane fade">
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="footer_visibility" class=" form-control-label">Visibility</label>
								</div>
								<div class="col-12 col-md-9">
									<input type="checkbox" name="footer_visibility" {{ ($pages->footer_visibility==1) ? 'checked' : '' }} value="1" data-toggle="toggle" data-width="100">
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="footer_text" class=" form-control-label">Footer HTML</label>
								</div>
								<div class="col-12 col-md-9">
									<textarea name="footer_text"  rows="9"  class="form-control ckeditor">{{$pages->footer_text}}</textarea>
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="copyright_text" class=" form-control-label">Copyright</label>
								</div>
								<div class="col-12 col-md-9">
									<textarea name="copyright_text"  rows="9"  class="form-control ckeditor">{{$pages->copyright_text}}</textarea>
								</div>
							</div>
						</div>
						<div id="menu2" class="tab-pane fade">
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="meta_keywords_seo" class=" form-control-label">Meta Keywords</label>
								</div>
								<div class="col-12 col-md-9">
									<input type="text" name="meta_keywords_seo"  rows="9"  class="form-control" value="{{$pages->meta_keywords_seo}}" placeholder="meta keywords for seo"><br>
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="meta_description_seo" class=" form-control-label">Meta Description</label>
								</div>
								<div class="col-12 col-md-9">
									<input type="text" name="meta_description_seo"  rows="9"  class="form-control" value="{{$pages->meta_description_seo}}" placeholder="meta description for seo"><br>
								</div>
							</div>
						</div>
						<button type="submit" class="btn btn-primary btn-sm">
							<i class="fa fa-dot-circle-o"></i> Update
						</button>
					</div>
				</div>
			</div>
		</form>
		@endif
		<div class="card-footer">
			<div class="row">
				<!-- <div class="col-md-6"> -->
					<div class="col-md-12 help-image">

					</div>
				</div>
			</div>
		</div>
	</div>

</div><!--/.col-->

@endsection