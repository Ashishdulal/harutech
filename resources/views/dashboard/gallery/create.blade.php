@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Home</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item">
						<a href="/backoffice/gallery">Portfolio</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">create</li>
				</ul>
			</div>
		</div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
		<div class="card-body card-block">
			<form action="/backoffice/gallery/create" method="POST" class="form-horizontal" enctype="multipart/form-data">
				@csrf
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="name" class=" form-control-label">Name</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="name" id="name" name="name" placeholder="Enter Name..." class="form-control">

					</div>
				</div>
				<div class="row form-group">
					<label class="col-md-3 control-label" for="image">Choose Gallery Category:</label>
					<div class="col-md-9">
						<select name="cat_id" required class="form-control">

							<option value="">---</option>
							@foreach($gallerycategories as $gall_cat)

							<option value="{{$gall_cat->id}}">{{$gall_cat->title}}</option>

							@endforeach
						</select>
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="image" class=" form-control-label">Image input</label>
					</div>
					<div class="col-12 col-md-9">
						<img id="blah" src="#" alt="Selected Image" />
						<input type="file" onchange="readURL(this);" id="image" accept="image/png, image/jpg, image/jpeg, video/mp4" name="image" class="form-control-file">
					</div>
				</div>
				<button type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-dot-circle-o"></i> Submit
				</button>
				<button type="reset" class="btn btn-danger btn-sm">
					<i class="fa fa-ban"></i> Reset
				</button>
			</form>
		</div>
		<div class="card-footer">

		</div>
	</div>
</div>



</div><!--/.col-->
@endsection