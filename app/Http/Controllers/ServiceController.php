<?php

namespace App\Http\Controllers;

use App\Mainpage;
use File;
use App\Jobs_detail;
use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $serviceBanner = Jobs_detail::findOrFail('1');
        $services = Service::all();
        return view ('dashboard.services.index',compact('services','serviceBanner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $services = new Service();
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $services->name = $request->name;
        $services->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = $request->file('image');
            $iimageName =  "services".time().'.'.$request->file('image')->getClientOriginalName();
            $image->move(public_path('uploads'),$iimageName);
            $services->image = $iimageName;
        }
        else{
            $services->image = 'default-thumbnail.png';
        }        
        $services->save();
        return redirect('/backoffice/services');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service,$id)
    {
        $services = service::findOrFail($id);
        return view ('dashboard.services.edit',compact('services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service,$id)
    {
        $services = Service::findOrFail($id);
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $services->name = $request->name;
        $services->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = $request->file('image');
            $iimageName =  "services".time().'.'.$request->file('image')->getclientOriginalName();
            $image->move(public_path('uploads'),$iimageName);
            $services->image = $iimageName;
        }
        else{
            $services->image = $services->image;
        }        
        $services->save();
        return redirect('/backoffice/services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy( Service $service,$id)
    {
        $services = Service::findOrFail($id);
        $image_path = public_path("uploads/".$services->image);
        if(file_exists($image_path)){
            //File::delete($image_path);
            File::delete( $image_path);
        }
        $services->delete();
        return redirect()->back();
    }
}
